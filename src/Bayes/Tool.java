package Bayes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class Tool {
	static ArrayList<String> stopWords=new ArrayList<String>();
	
	static{
		try {
			@SuppressWarnings("resource")
			BufferedReader in=new BufferedReader(new InputStreamReader(
					new FileInputStream(new File("./File/stopwords.txt")),"utf-8"));
			String temp=null;
			while((temp=in.readLine())!=null){
				Tool.stopWords.add(temp);
			}
		} catch (UnsupportedEncodingException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}
	
	public static String[] splitStopWords(String[] text){
		String result="";
		for(int x=0;x<text.length;x++){
			if(Tool.stopWords.contains(text[x]))
				continue;
			else
				result=result+text[x]+",";
		}
		return result.split(",");
	}
}
