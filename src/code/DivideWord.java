package code;

import java.io.IOException;

import code.NlpirTest.CLibrary;


public class DivideWord {
	
	public static void main(String[] args){
		String[] temp=divideWord("冤 死 ， 什么 法治 社会 ， 应该 把 国家 告 了 ， 法院 告 了 ， 审判长 及 一 帮子 人 都 告 一下",0);
		for(int x=0;x<temp.length;x++){
			System.out.print(temp[x]+'\t');
		}
	}

	public static String[] divideWord(String resource,int wordTpye){
		int init_flag = CLibrary.Instance.NLPIR_Init("",1, "0");//默认为GBK，0：GBK；1：UTF-8;2:BIG5;3:GBK_FANTI
		
		if (0 == init_flag) {
			String nativeBytes = CLibrary.Instance.NLPIR_GetLastErrorMsg();
			System.err.println("初始化失败！fail reason is "+nativeBytes);
			return null;
		}
		String temp=CLibrary.Instance.NLPIR_ParagraphProcess(resource, wordTpye);
		String[] result=temp.split("   ");
		return result;
	}

}
